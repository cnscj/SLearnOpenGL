//
//  GLSApplicationProtocol.cpp
//  OpenGLDemo
//
//  Created by 石超军 on 2018/7/4.
//  Copyright © 2018年 石超军. All rights reserved.
//
#include <glad/glad.h>
#include "GLSApplicationProtocol.h"
#include <iostream>
std::map<GLFWwindow*,GLSApplicationProtocol*> GLSApplicationProtocol::sm_appsMap;

GLSApplicationProtocol::GLSApplicationProtocol()
{
    if(!glfwInit())
    {
        std::cout << "Failed to initialize GLFW" << std::endl;
        return ;
    }
    
    
    /*  FIXME:以下代码有误 */
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);//使用核心模式绘图-简单绘图不起作用

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
#endif
    
    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return ;
    }

    
}
GLSApplicationProtocol::~GLSApplicationProtocol()
{
    
}

void GLSApplicationProtocol::OnSizeChanged(int width, int height)
{

}
void GLSApplicationProtocol::OnMouseButton(int button, int action, int mods)
{
    
}
void GLSApplicationProtocol::OnMouseScroll(double x, double y)
{
    
}

void GLSApplicationProtocol::OnMouse(double xpos, double ypos)
{
    
}
//键盘事件
void GLSApplicationProtocol::OnKeyboard(int,int,int,int)
{
    
}

void GLSApplicationProtocol::_AddApp(GLFWwindow* window,GLSApplicationProtocol* app)
{
    glfwSetFramebufferSizeCallback(window, __OnFrameBufferSizeCallback);
    glfwSetMouseButtonCallback(window, __OnFrameMouseButtonCallback);
    glfwSetScrollCallback(window, __OnFrameMouseScrollCallback);
    glfwSetCursorPosCallback(window, __OnFrameMouseCallback);
    glfwSetKeyCallback(window, __OnFrameKeyboardCallback);
    
    sm_appsMap.insert(std::pair<GLFWwindow*,GLSApplicationProtocol*>(window,app));
}

void GLSApplicationProtocol::_RemoveApp(GLFWwindow* window)
{
    sm_appsMap.erase(window);
}
GLSApplicationProtocol* GLSApplicationProtocol::_GetApp(GLFWwindow* window)
{
    auto it = sm_appsMap.find(window);
    if (it != sm_appsMap.end())
        return it->second;
    return nullptr;
}


void GLSApplicationProtocol::__OnFrameBufferSizeCallback(GLFWwindow* window, int width, int height)
{
    auto app = _GetApp(window);
    if(app) app->OnSizeChanged(width,height);
}
void GLSApplicationProtocol::__OnFrameMouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
    auto app = _GetApp(window);
    if(app) app->OnMouseButton(button,action,mods);
}
void GLSApplicationProtocol::__OnFrameMouseScrollCallback(GLFWwindow* window, double x, double y)
{
    auto app = _GetApp(window);
    if(app) app->OnMouseScroll(x,y);
}
void GLSApplicationProtocol::__OnFrameMouseCallback(GLFWwindow* window, double xpos, double ypos)
{
    auto app = _GetApp(window);
    if(app) app->OnMouse(xpos,ypos);
}

void GLSApplicationProtocol::__OnFrameKeyboardCallback(GLFWwindow* window, int key,int scancode,int action,int mods)
{
    auto app = _GetApp(window);
    if(app) app->OnKeyboard(key,scancode,action,mods);
}
