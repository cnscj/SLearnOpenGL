#ifndef MESH_H
#define MESH_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>

#include "CShader.h"

struct VERTEX_INFO {
    // position
    glm::vec3 Position;
    // normal
    glm::vec3 Normal;
    // texCoords
    glm::vec2 TexCoords;
    // tangent
    glm::vec3 Tangent;
    // bitangent
    glm::vec3 Bitangent;
};

struct TEXTURE_INFO {
    unsigned int id;
    std::string type;
    std::string path;
};

class CMesh {
public:
    /*  Mesh Data  */
    std::vector<VERTEX_INFO> vertices;
    std::vector<unsigned int> indices;
    std::vector<TEXTURE_INFO> textures;
    unsigned int VAO;
    
    /*  Functions  */
    // constructor
    CMesh(std::vector<VERTEX_INFO> vertices, std::vector<unsigned int> indices, std::vector<TEXTURE_INFO> textures);
    
    // render the mesh
    void Draw(CShader& shader);
    
private:
    /*  Render data  */
    unsigned int VBO, EBO;
    
    /*  Functions    */
    // initializes all the buffer objects/arrays
    void SetupMesh();
};
#endif
