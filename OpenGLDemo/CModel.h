#ifndef MODEL_H
#define MODEL_H



#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "CMesh.h"
#include "CShader.h"

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <vector>


unsigned int TextureFromFile(const char *path, const std::string &directory, bool gamma = false);

class CModel
{
public:
    /*  Model Data */
    std::vector<TEXTURE_INFO> textures_loaded;    // stores all the textures loaded so far, optimization to make sure textures aren't loaded more than once.
    std::vector<CMesh> meshes;
    std::string directory;
    bool gammaCorrection;
    
    /*  Functions   */
    // constructor, expects a filepath to a 3D model.
    CModel(std::string const &path, bool gamma = false);
    
    // draws the model, and thus all its meshes
    void Draw(CShader& shader);
    
private:
    /*  Functions   */
    // loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes vector.
    void LoadModel(std::string const &path);
    
    // processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
    void ProcessNode(aiNode *node, const aiScene *scene);
    
    CMesh ProcessMesh(aiMesh *mesh, const aiScene *scene);
    
    // checks all material textures of a given type and loads the textures if they're not loaded yet.
    // the required info is returned as a Texture struct.
    std::vector<TEXTURE_INFO> LoadMaterialTextures(aiMaterial *mat, aiTextureType type, std::string typeName);
};

#endif
