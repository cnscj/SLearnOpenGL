

#version 330 core

struct Material {
    sampler2D diffuse;
    sampler2D specular;
    float     shininess;
};

struct Light {
    vec4 lightVector;
    
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    
    //衰减常量
    float constant;
    float linear;
    float quadratic;
};

out vec4 FragColor;

in vec3 Normal;
in vec3 FragPos;
in vec2 TexCoords;

uniform vec3 viewPos;

uniform Material material;
uniform Light light;

void main()
{
    // 环境光
    vec3 ambient  = light.ambient  * vec3(texture(material.diffuse, TexCoords));
    
    // 漫反射
    vec3 norm = normalize(Normal);// 执行定向光照计算
    vec3 lightDir;
    
    if(light.lightVector.w == 0.0)     //位置光
        lightDir = normalize(vec3(light.lightVector) - FragPos);
    else if(light.lightVector.w == 1.0)// 定向光
        lightDir = normalize(-vec3(light.lightVector));
        
        float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse  = light.diffuse  * diff * vec3(texture(material.diffuse, TexCoords));
    
    // 镜面光
    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    vec3 specular = light.specular * spec * vec3(texture(material.specular, TexCoords));
    
    float distance    = length(vec3(light.lightVector) - FragPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
    
    ambient  *= attenuation;
    diffuse  *= attenuation;
    specular *= attenuation;
    
    vec3 result = ambient + diffuse + specular;
    FragColor = vec4(ambient + diffuse + specular, 1.0);
}

