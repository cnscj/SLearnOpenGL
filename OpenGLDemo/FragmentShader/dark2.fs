#version 330 core
out vec4 FragColor;

in vec3 ourColor;
in vec2 TexCoord;

uniform sampler2D texture1;
uniform sampler2D texture2;


void main()
{
    FragColor = mix(texture(texture1, TexCoord), texture(texture2, TexCoord), 0.2);
    if (TexCoord.x > 0.5)
    {
        FragColor.xyz = vec3(0.4*FragColor.r + 0.4*FragColor.g +0.4*FragColor.b);
        FragColor.w = FragColor.w;
    }
    else
    {
        FragColor.xyz = FragColor.xyz;
        FragColor.w = FragColor.w;
    }
    
}


