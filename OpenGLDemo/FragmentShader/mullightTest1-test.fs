
#version 330 core
out vec4 FragColor;

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;

void main()
{
    FragColor = vec4(1.0); // 将向量的四个分量全部设置为1.0
}
