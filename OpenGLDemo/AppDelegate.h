//
//  AppDelegate.hpp
//  OpenGLDemo
//
//  Created by 石超军 on 2018/7/6.
//  Copyright © 2018年 石超军. All rights reserved.
//

#ifndef AppDelegate_hpp
#define AppDelegate_hpp

#include <functional>
#include <iostream>

#include "GLSApplication.h"
#include "CCamera.h"
class AppDelegate : public GLSApplication
{
protected:
    
    void _CoordTest1();
    void _CoordTest2();
    void _CoordTest3();
    void _OtherCoord();
    
    void _ColorTest1();
    void _LightTest1();
    void _LightTest2();
    void _LightTest3();
  
    void _MateriaTest1();
    void _MateriaTest2();
    
    void _LightTextureTest1();
    void _LightTextureTest2();
    
    void _LightCasterTest1();
    void _LightCasterTest2();
    void _LightCasterTest3();
    
    void _MulLightTest1();
    void _MulLightTest2();
    
    void _ModelTest1();
protected:
    virtual void OnInit();
    virtual void OnInput();
    virtual void OnRender();
private:
    CCamera m_camera;
    
};

#endif /* AppDelegate_hpp */
