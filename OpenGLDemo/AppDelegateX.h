//
//  AppDelegate.hpp
//  OpenGLDemo
//
//  Created by 石超军 on 2018/7/4.
//  Copyright © 2018年 石超军. All rights reserved.
//

#ifndef _APPDELEGATEX_H_
#define _APPDELEGATEX_H_
#include <functional>
#include <iostream>
#include "GLSApplication.h"

class AppDelegateX : public GLSApplication
{
    typedef std::function<void(unsigned int,unsigned int,unsigned int)> rendercall;
    typedef std::function<void(unsigned int,unsigned int,unsigned int)> vertexcall;
    typedef std::function<void(std::function<unsigned int(const char *)>)> texturecall;

protected:
    void _TriangleTest();
    void _ShaderTest1();
    void _ShaderTest2();
    void _ShaderTest3();
    void _ShaderTest4();
    void _TextureTest1();
    void _TextureTest2();
    void _Transform1();
    void _DarkTest1();
    void _DarkTest2();
    void _CoordTest1();
protected:
    void _OldTriangleTest();
protected:
    virtual void OnInit();
    virtual void OnRender();
protected:
    void Render(const float vertices[],
                unsigned int verticesLen,
                const char *vertexSource,
                const char *fragmentSource,
                const rendercall *draw);
    void Render(const vertexcall *vertexCall,
                const char *vertexSource,
                const char *fragmentSource,
                const rendercall *drawCall);
    
    void RenderT(
                 const char *textureFilePath,
                 const char *vertexFilePath,
                 const char *fragmentFilePath,
                 const vertexcall *vertexCall,
                 const rendercall *drawCall);
    void RenderT2(
                  const char *vertexFilePath,
                  const char *fragmentFilePath,
                  const vertexcall *vertexCall,
                  const texturecall *textureCall,
                  const rendercall *drawCall);
    
};

#endif /* AppDelegate_hpp */
