#ifndef _GLSAPPLICATION_H_
#define _GLSAPPLICATION_H_

#include "GLSApplicationProtocol.h"

class GLSApplication : public GLSApplicationProtocol
{
public:
    //运行
    int Run();
    //创建
    int Create(int width, int height, const char* title, GLFWmonitor* monitor=NULL, GLFWwindow* share=NULL);
    //取得窗口句柄
    GLFWwindow* GetWindowHandle();
    
    int GetWidth();
    
    int GetHeight();
    
    void SetState(unsigned int);
protected:
    //数据初始化
    virtual void OnInit();
    //渲染回调
    virtual void OnRender();
    //输入回调
    virtual void OnInput();
    //窗口变化回调
    virtual void OnSizeChanged(int width, int height);
protected:
    GLSApplication();
    ~GLSApplication();
private:
    GLFWwindow* m_pWindow;
};

#endif

