#ifndef TEXTURE2D_H
#define TEXTURE2D_H

#include <GLFW/glfw3.h>

struct gl_texture_t
{
    GLsizei width;
    GLsizei height;
    GLenum format;
    GLint internalFormat;
    GLuint id;
    GLubyte* texels;
};

class Texture2D
{
public:
    Texture2D();

public:
    //从文件中加载
    GLuint loadFromFile(const char* path);
    GLuint loadFromFile2(const char* path);
    gl_texture_t *readPNGFromFile(const char* filename);
    void getPNGtextureInfo(int color_type, gl_texture_t *texinfo);
public:
    void redner();
};

#endif // TEXTURE2D_H
