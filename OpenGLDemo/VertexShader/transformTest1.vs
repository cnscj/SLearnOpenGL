
#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
layout (location = 2) in vec2 aTexCoord;
//作为片段着色器的输入
out vec3 ourColor;
out vec2 TexCoord;

uniform mat4 transform;

void main()
{
    gl_Position = transform * vec4(aPos, 1.0f); //矩阵与坐标相乘,得到新的坐标
    TexCoord = vec2(aTexCoord.x, 1.0 - aTexCoord.y);    //翻转纹理Y轴坐标,这样纹理绘制出来是上下颠倒的
    ourColor = aColor;

}

