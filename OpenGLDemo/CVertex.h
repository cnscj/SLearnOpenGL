//
//  CBuffer.hpp
//  OpenGLDemo
//
//  Created by 石超军 on 2018/7/6.
//  Copyright © 2018年 石超军. All rights reserved.
//

#ifndef CBuffer_hpp
#define CBuffer_hpp
#include <iostream>
#include<functional>
#include <GLFW/glfw3.h>

struct CVERTEX_INFO
{
    unsigned int patches;
    unsigned int elements;
};

struct CVERTEX_DATA
{
    struct CVERTEX_META
    {
        unsigned int len;
        float *data;
    };
    
    struct CINDEX_META
    {
        unsigned int len;
        unsigned int *data;
    };
    
    unsigned int format;
    
    CVERTEX_META vertices;
    CINDEX_META indices;
    
};

class CVertex
{
public:
    static const unsigned int FVF_POS;
    static const unsigned int FVF_COLOR;
    static const unsigned int FVF_NORMAL;
    static const unsigned int FVF_TEX2D;
private:
    static const unsigned int FVF_BIT[];
public:
    CVertex(unsigned int vFormat,CVERTEX_INFO* info=nullptr,const float vertices[] = nullptr,unsigned int verticesLen=-1,
            const unsigned int indices[]=nullptr,unsigned int indicesLen=-1);
    CVertex(std::function<void(unsigned int,unsigned int,unsigned int)>);
    CVertex(std::string path);
    CVertex(const CVERTEX_DATA *info);
    ~CVertex();
public:
    bool LoadFromParams(unsigned int vFormat,CVERTEX_INFO* info=nullptr,const float vertices[] = nullptr,unsigned int verticesLen=-1,
                        const unsigned int indices[]=nullptr,unsigned int indicesLen=-1);
    bool LoadFromFile(const char* file);
    void Bind();
    void Draw();
private:
    unsigned int m_VAO;
    unsigned int m_VBO;
    unsigned int m_EBO;
};

#endif /* CBuffer_hpp */
