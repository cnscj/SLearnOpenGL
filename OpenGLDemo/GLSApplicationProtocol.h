//
//  GLSApplicationProtocol.hpp
//  OpenGLDemo
//
//  Created by 石超军 on 2018/7/4.
//  Copyright © 2018年 石超军. All rights reserved.
//

#ifndef GLSAPPLICATIONPROTOCOL_H_
#define GLSAPPLICATIONPROTOCOL_H_
#include <map>


#include <GLFW/glfw3.h>

class GLSApplicationProtocol
{
protected:
    GLSApplicationProtocol();
    ~GLSApplicationProtocol();
protected:
    //窗口变化回调
    virtual void OnSizeChanged(int width, int height);
    //鼠标事件
    virtual void OnMouseButton(int button, int action, int mods);
    virtual void OnMouseScroll(double x, double y);
    virtual void OnMouse(double xpos, double ypos);
    //键盘事件
    virtual void OnKeyboard(int key,int scancode,int action,int mods);
protected:
    static void _AddApp(GLFWwindow*,GLSApplicationProtocol*);
    static void _RemoveApp(GLFWwindow*);
    static GLSApplicationProtocol* _GetApp(GLFWwindow*);
private:
    static std::map<GLFWwindow*,GLSApplicationProtocol*> sm_appsMap;
private:
    static void __OnFrameBufferSizeCallback(GLFWwindow* window, int width, int height);
    static void __OnFrameMouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
    static void __OnFrameMouseScrollCallback(GLFWwindow* window, double x, double y);
    static void __OnFrameMouseCallback(GLFWwindow* window, double xpos, double ypos);
    static void __OnFrameKeyboardCallback(GLFWwindow* window, int,int,int,int);
};

#endif /* GLSApplicationProtocol_hpp */
