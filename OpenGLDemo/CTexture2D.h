//
//  CTexture.hpp
//  OpenGLDemo
//
//  Created by 石超军 on 2018/7/6.
//  Copyright © 2018年 石超军. All rights reserved.
//

#ifndef _CTEXTURE2D_H_
#define _CTEXTURE2D_H_
#include <iostream>

#include <GLFW/glfw3.h>

struct CTEXTURE2D_INFO
{
    GLsizei width;      //宽
    GLsizei height;     //高
    GLsizei channels;   //通道数
};

class CTexture2D
{
public:
    CTexture2D(const char* path,CTEXTURE2D_INFO* info=nullptr);
    ~CTexture2D();
public:
    //从文件中加载
    bool LoadFromFile(const char* path,CTEXTURE2D_INFO* info);
    void Active(unsigned int unit = GL_TEXTURE0);
    unsigned int GetId();
public:
    unsigned int  m_textureId;
};

#endif /* CTexture_hpp */
