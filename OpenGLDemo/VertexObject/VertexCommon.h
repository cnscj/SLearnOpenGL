#include "../CVertex.h"

extern CVERTEX_DATA __CreateGLSphere(int m,int n)
{
    CVERTEX_DATA data;
    const float PI = 3.1415926f;
    const float PI2 = 6.2831853f;
    unsigned int vertNum=m*n*4;//顶点总数
    unsigned int len = vertNum*(3+2);
    float *vertices = new float[len];
    
    float stepAngZ=PI/m;//纵向角度每次增加的值
    float stepAngXY=PI2/n;//横向角度每次增加的值
    float angZ=0.0;//初始的纵向角度
    float angXY=0.0;//初始的横向角度
    
    int index=0;
    for(int i=0;i<m;i++) {
        for(int j=0;j<n;j++) {
            //构造一个顶点
            float x1=sin(angZ)*cos(angXY);
            float y1=sin(angZ)*sin(angXY);
            float z1=cos(angZ);
            vertices[index]=x1; index++;
            vertices[index]=y1; index++;
            vertices[index]=z1; index++;
            float v1=angZ/PI;
            float u1=angXY/PI2;
            vertices[index]=u1; index++;
            vertices[index]=v1; index++;
            
            float x2=sin(angZ+stepAngZ)*cos(angXY);
            float y2=sin(angZ+stepAngZ)*sin(angXY);
            float z2=cos(angZ+stepAngZ);
            vertices[index]=x2; index++;
            vertices[index]=y2; index++;
            vertices[index]=z2; index++;
            float v2=(angZ+stepAngZ)/PI;
            float u2=angXY/PI2;
            vertices[index]=u2; index++;
            vertices[index]=v2; index++;
            
            
            float x3=sin(angZ+stepAngZ)*cos(angXY+stepAngXY);
            float y3=sin(angZ+stepAngZ)*sin(angXY+stepAngXY);
            float z3=cos(angZ+stepAngZ);
            vertices[index]=x3; index++;
            vertices[index]=y3; index++;
            vertices[index]=z3; index++;
            float v3=(angZ+stepAngZ)/PI;
            float u3=(angXY+stepAngXY)/PI2;
            vertices[index]=u3; index++;
            vertices[index]=v3; index++;
            
            float x4=sin(angZ)*cos(angXY+stepAngXY);
            float y4=sin(angZ)*sin(angXY+stepAngXY);
            float z4=cos(angZ);
            vertices[index]=x4; index++;
            vertices[index]=y4; index++;
            vertices[index]=z4; index++;
            float v4=angZ/PI;
            float u4=(angXY+stepAngXY)/PI2;
            vertices[index]=u4; index++;
            vertices[index]=v4; index++;
            
            angXY+=stepAngXY;
        }
        angXY=0.0;//每次横向到达2PI角度则横向角度归0
        angZ+=stepAngZ;
    }

    data.format = CVertex::FVF_POS | CVertex::FVF_TEX2D;
    data.vertices.data = vertices;
    data.vertices.len = len*sizeof(float);
    data.indices.data = nullptr;
    data.indices.len = 0;
    return data;
}


extern const float __VERTEX_CUBE_V_DATA[] = {
    -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
    0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
    0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
    0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
    
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
    0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
    0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
    0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
    -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
    
    -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    
    0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
    0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
    0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
    0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
    0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
    0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
    0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
    0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
    
    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
    0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
    0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
};

//带法向量的正方体
extern const float __VERTEX_CUBE_N_V_DATA[] = {
    -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
    0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
    0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
    0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
    -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
    
    -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
    0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
    0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
    0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
    -0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
    
    -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
    -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
    -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
    -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
    -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
    -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
    
    0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
    0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
    0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
    0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
    0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
    0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
    
    -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
    0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
    0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
    0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
    
    -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
    0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
    0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
    0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
    -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
    -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f
};

//带纹理坐标和法向量的正方体
extern const float __VERTEX_CUBE_PTN_VDATA[] = {
    // positions          // normals           // texture coords
    -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,
    0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 0.0f,
    0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,
    0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,
    -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,
    
    -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 0.0f,
    0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 0.0f,
    0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 1.0f,
    0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 1.0f,
    -0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 0.0f,
    
    -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
    -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 1.0f,
    -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 0.0f,
    -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
    
    0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
    0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f,
    0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
    0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
    0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 0.0f,
    0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
    
    -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f,
    0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 1.0f,
    0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 0.0f,
    0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 0.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 0.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f,
    
    -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f,
    0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 1.0f,
    0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
    0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
    -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 0.0f,
    -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f
};
    
extern const float __VERTEX_SQUARE_V_DATA[] = {
    //     ---- 位置 ----       ---- 颜色 ----     - 纹理坐标 -
    0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f,   // 右上
    0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f,   // 右下
    -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f,   // 左下
    -0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f    // 左上
};


extern const unsigned int __VERTEX_SQUARE_I_DATA[] = {
    0, 1, 3, // first triangle
    1, 2, 3  // second triangle
};

extern const CVERTEX_DATA MODEL_CUBE = {
                            .format = CVertex::FVF_POS|CVertex::FVF_TEX2D,
                            .vertices.len = sizeof(__VERTEX_CUBE_V_DATA),
                            .vertices.data = (float*)__VERTEX_CUBE_V_DATA,
                            .indices.len = 0,
                            .indices.data = nullptr
                        };

extern const CVERTEX_DATA MODEL_CUBE_N = {
    .format = CVertex::FVF_POS|CVertex::FVF_NORMAL,
    .vertices.len = sizeof(__VERTEX_CUBE_N_V_DATA),
    .vertices.data = (float*)__VERTEX_CUBE_N_V_DATA,
    .indices.len = 0,
    .indices.data = nullptr
};

extern const CVERTEX_DATA MODEL_CUBE_PTN = {
    .format = CVertex::FVF_POS|CVertex::FVF_NORMAL|CVertex::FVF_TEX2D,
    .vertices.len = sizeof(__VERTEX_CUBE_PTN_VDATA),
    .vertices.data = (float*)__VERTEX_CUBE_PTN_VDATA,
    .indices.len = 0,
    .indices.data = nullptr
};

extern const CVERTEX_DATA MODEL_SQUARE = {
    .format = CVertex::FVF_POS|CVertex::FVF_COLOR|CVertex::FVF_TEX2D,
    .vertices.len = sizeof(__VERTEX_SQUARE_V_DATA),
    .vertices.data = (float*)__VERTEX_SQUARE_V_DATA,
    .indices.len = sizeof(__VERTEX_SQUARE_I_DATA),
    .indices.data = (unsigned int*)__VERTEX_SQUARE_I_DATA
};

extern const CVERTEX_DATA MODEL_SPHERE = __CreateGLSphere(1,1);
